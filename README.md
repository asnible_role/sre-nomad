Role Name
=========

A brief description of the role goes here.

Requirements
------------

None.

Role Variables
--------------

A description of the settable variables for this role should go here, including any variables that are in defaults/main.yml, vars/main.yml, and any variables that can/should be set via parameters to the role. Any variables that are read from other roles and/or the global scope (ie. hostvars, group vars, etc.) should be mentioned here as well.

Dependencies
------------

None.

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:
```yaml
- hosts: all
  become: yes
  roles:
    - sre-nomad
```
Example inventory
----------------
```ini
[nomad]
node-1  ansible_host=192.168.1.61
node-2  ansible_host=192.168.1.254
node-3  ansible_host=192.168.1.252

[nomad:var]
ansible_user=anton
```
License
-------

BSD

Author Information
------------------

An optional section for the role authors to include contact information, or a website (HTML is not allowed).
